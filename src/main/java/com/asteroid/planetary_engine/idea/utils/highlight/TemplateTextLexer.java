package com.asteroid.planetary_engine.idea.utils.highlight;

import com.intellij.lexer.FlexAdapter;

/**
 * @author shuyang
 * @since 2024/4/28 15:40
 */
public class TemplateTextLexer extends FlexAdapter {
    TemplateTextLexer() {
        super(new _TemplateTextLexer());
    }
}