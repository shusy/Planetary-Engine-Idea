package com.asteroid.planetary_engine.idea.utils;

import com.intellij.notification.Notification;
import com.intellij.notification.NotificationGroup;
import com.intellij.notification.NotificationGroupManager;
import com.intellij.notification.Notifications;
import com.intellij.openapi.ui.MessageType;

/**
 * @author shuyang
 * @since 2024/4/14 17:05
 */
public class NotifyUtil {

    private static final NotificationGroup entityGenerateNotifyGroup = NotificationGroupManager.getInstance().getNotificationGroup("EntityGenerateNotifyGroup");

    public static void notify(String title) {
        notify(title, MessageType.INFO);
    }

    public static void notify(String title, MessageType messageType) {
        Notification notification = entityGenerateNotifyGroup.createNotification(title, messageType);
        Notifications.Bus.notify(notification);
    }
}
