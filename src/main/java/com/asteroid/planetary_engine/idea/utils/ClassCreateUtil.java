package com.asteroid.planetary_engine.idea.utils;

import com.intellij.ide.highlighter.JavaFileType;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.MessageType;
import com.intellij.openapi.ui.Messages;
import com.intellij.psi.*;
import com.intellij.util.ui.UIUtil;

import java.util.concurrent.atomic.AtomicReference;

public class ClassCreateUtil {

    private static PsiDirectory createNestedDirectories(PsiDirectory baseDirectory, String[] packageParts) {
        AtomicReference<PsiDirectory> currentDirectory = new AtomicReference<>(baseDirectory);
        ApplicationManager.getApplication().runWriteAction(() -> {
            currentDirectory.set(baseDirectory);
            for (String packageNamePart : packageParts) {
                PsiDirectory subdirectory = currentDirectory.get().findSubdirectory(packageNamePart);
                if (subdirectory == null) {
                    subdirectory = currentDirectory.get().createSubdirectory(packageNamePart);
                }
                currentDirectory.set(subdirectory);
            }
        });
        return currentDirectory.get(); // 返回最终创建或找到的目录
    }

    private static PsiDirectory findJavaDir(PsiDirectory directory) {
        if (directory.getName().equals("java")) {
            return directory;
        }
        PsiDirectory parentDirectory = directory.getParentDirectory();
        if (parentDirectory == null) {
            return null;
        }
        return findJavaDir(parentDirectory);
    }

    public static PsiFile generateClassFile(Project project, String content, String targetFileName, String realPackageName, PsiFile psiFile) {
        PsiDirectory javaDir = findJavaDir(psiFile.getContainingDirectory());
        if (javaDir == null) {
            NotifyUtil.notify("未找到java目录", MessageType.WARNING);
            Messages.showMessageDialog("未找到java根目录", "生成" + targetFileName + "失败", UIUtil.getWarningIcon());
            return psiFile;
        }
        PsiDirectory nestedDirectories = createNestedDirectories(javaDir, realPackageName.split("\\."));
        if (nestedDirectories.findFile(targetFileName) != null) {
            NotifyUtil.notify("文件：" + targetFileName + "已存在", MessageType.WARNING);
            return psiFile;
        }
        PsiFile targetFile = PsiFileFactory.getInstance(project).createFileFromText(targetFileName, JavaFileType.INSTANCE, content);
        WriteCommandAction.runWriteCommandAction(project, () -> {
            nestedDirectories.add(targetFile);
        });
        return targetFile;
    }

    public static void generateClassFile(Project project, PsiClass psiClass) {
        String sourceName = psiClass.getName();
        if (sourceName == null) {
            NotifyUtil.notify("未找到文件名", MessageType.WARNING);
            return;
        }
        String className = sourceName + "Mapper";
        String targetFileName = className + ".java";
        String sourceQualifiedName = psiClass.getQualifiedName();
        PsiJavaFile psiJavaFile = (PsiJavaFile) psiClass.getContainingFile();
        String sourcePackageName = psiJavaFile.getPackageName();
        String packageName = sourcePackageName.substring(0, psiJavaFile.getPackageName().lastIndexOf(".")) + ".mapper";
        PsiDirectory parentDirectory = psiClass.getContainingFile().getContainingDirectory().getParentDirectory();
        assert parentDirectory != null;
        PsiDirectory workDir = parentDirectory.findSubdirectory("mapper");
        if (workDir == null) {
            workDir = parentDirectory.createSubdirectory("mapper");
        }
        if (workDir.findFile(targetFileName) != null) {
            NotifyUtil.notify(targetFileName + "文件已存在", MessageType.WARNING);
            Messages.showMessageDialog(targetFileName + "文件已存在", "生成mapper失败", UIUtil.getWarningIcon());
            return;
        }
        String content = """
                package ${packageName};
                                
                import com.baomidou.mybatisplus.core.mapper.BaseMapper;
                import org.apache.ibatis.annotations.Mapper;
                import ${sourceQualifiedName};
                                
                @Mapper
                public interface ${className} extends BaseMapper<${sourceName}>  {
                                
                }           
                """;
        String targetContent = content
                .replace("${packageName}", packageName)
                .replace("${className}", className)
                .replace("${sourceQualifiedName}", sourceQualifiedName)
                .replace("${sourceName}", sourceName);
        PsiDirectory finalWorkDir = workDir;
        WriteCommandAction.runWriteCommandAction(project, () -> {
            PsiFile psiFile = PsiFileFactory.getInstance(project).createFileFromText(targetFileName, JavaFileType.INSTANCE, targetContent);
            finalWorkDir.add(psiFile);
        });
    }
}
