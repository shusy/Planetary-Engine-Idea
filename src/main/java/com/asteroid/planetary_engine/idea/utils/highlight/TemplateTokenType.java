package com.asteroid.planetary_engine.idea.utils.highlight;

import com.intellij.lang.Language;
import com.intellij.psi.tree.IElementType;

/**
 * @author shuyang
 * @since 2024/4/28 15:41
 */
public interface TemplateTokenType {
    IElementType TEXT = new IElementType("TEXT", Language.ANY);
    IElementType VARIABLE = new IElementType("VARIABLE", Language.ANY);
    IElementType ESCAPE_DOLLAR = new IElementType("ESCAPE_DOLLAR", Language.ANY);
    IElementType MACRO = new IElementType("MACRO", Language.ANY);
    IElementType DIRECTIVE = new IElementType("DIRECTIVE", Language.ANY);
}
