package com.asteroid.planetary_engine.idea.template;

import com.intellij.codeInsight.template.Template;
import com.intellij.codeInsight.template.TemplateManager;
import com.intellij.codeInsight.template.postfix.templates.*;
import com.intellij.codeInsight.template.postfix.util.JavaPostfixTemplatesUtils;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.Condition;
import com.intellij.openapi.util.TextRange;
import com.intellij.psi.*;
import com.intellij.psi.codeStyle.CodeStyleManager;
import org.jetbrains.annotations.NotNull;

/**
 * @author shuyang
 * @since 2024/7/18 21:57
 */
public class GenerateLogPostfixTemplate extends PostfixTemplateWithExpressionSelector {
    /**
     * which condition can use this template.
     */
    public static final Condition<PsiElement> IS_OK =
            psiElement -> {
                Project project = psiElement.getProject();
                PsiType type = ((PsiExpression) psiElement).getType();
                if (type == null) {
                    return false;
                }
                PsiClass psiClass = JavaPsiFacade.getInstance(project).findClass(type.getCanonicalText(), psiElement.getResolveScope());
                return psiClass != null;
            };

    public GenerateLogPostfixTemplate(PostfixTemplateProvider provider) {
        super(null, "log", "Generate log info", JavaPostfixTemplatesUtils.selectorTopmost(IS_OK), provider);
    }

    @Override
    public boolean isBuiltin() {
        return false;
    }

    @Override
    public boolean isEditable() {
        return false;
    }

    /**
     * reference:
     * com.intellij.codeInsight.template.postfix.templates.editable.EditablePostfixTemplate#expandForChooseExpression(PsiElement, Editor)
     *
     * @param expression the expression.
     * @param editor     the editor.
     */
    @Override
    protected void expandForChooseExpression(@NotNull PsiElement expression, @NotNull Editor editor) {
        // expression就是触发后缀表达式的地方，这里可以判断一下，比如当前行是否是空行，是否是注释行等等
        // PsiType type = ((PsiExpression) expression).getType();
        // assert type != null;
        // String className = type.getCanonicalText();
        // PsiClass psiClass = JavaPsiFacade.getInstance(expression.getProject()).findClass(className, expression.getResolveScope());
        // assert psiClass != null;

        // 截取字符串，生成新字符串，删除旧字符串
        Document document = editor.getDocument();
        String currentLineText = getCurrentLineText1(document, expression);
        // String[] split = currentLineText.split(",");
        final String afterText = buildTemplateString(currentLineText);
        document.deleteString(expression.getTextRange().getStartOffset(), expression.getTextRange().getEndOffset());
        Project project = expression.getProject();


        TemplateManager manager = TemplateManager.getInstance(project);
        Template template = manager.createTemplate(getId(), "", afterText);
        // template = modifyTemplate(template);
        template.setToReformat(true);
        manager.startTemplate(editor, template);
        PsiFile containingFile = PsiDocumentManager.getInstance(project).getPsiFile(editor.getDocument());
        PsiJavaFile javaFile = (PsiJavaFile) containingFile;
        // importPackage(editor, project);
    }


    /**
     * @see TryStatementPostfixTemplate#isApplicable(PsiElement, Document, int)
     */
    @Override
    public boolean isApplicable(@NotNull PsiElement context, @NotNull Document copyDocument, int newOffset) {
        // 父类的实现，默认会识别语法，读取变量
        return super.isApplicable(context, copyDocument, newOffset);

        // .try的实现，会识别到行尾，但是需要继承PostfixTemplate，而不是现在的PostfixTemplateWithExpressionSelector
        // PsiStatement statementParent = PsiTreeUtil.getNonStrictParentOfType(context, PsiStatement.class);
        // if (statementParent == null ||
        //         newOffset != statementParent.getTextRange().getEndOffset()) return false;
        //
        // if (statementParent instanceof PsiDeclarationStatement) return true;
        //
        // if (statementParent instanceof PsiExpressionStatement) {
        //     PsiExpression expression = ((PsiExpressionStatement) statementParent).getExpression();
        //     return null != expression.getType();
        // }
        //
        // return false;
    }

    public static void main(String[] args) {
        String[] strings = new String[]{"abc", "kkk"};
        System.out.println(buildTemplateString(strings));
    }

    public static String buildTemplateString(String str) {
        return "log.info(\"" + str + ":{}\", " + str + ");$END$";
    }

    public static String buildTemplateJsonString(String str) {
        return "log.info(\"$END$" + str + ":{}\", JSONUtil.toJsonStr(" + str + "));";
    }

    private static String buildTemplateString(String[] split) {
        StringBuilder builder = new StringBuilder("log.info(\"");
        for (int i = 0; i < split.length; i++) {
            String str = split[i];
            builder.append(str).append(":{}");
            if (i != split.length - 1) {
                builder.append(", ");
            }
        }
        builder.append("\"");
        for (String str : split) {
            builder.append(", ").append(str);
        }
        builder.append(");$END$");
        return builder.toString();
    }

    private static String getCurrentLineText1(Document document, PsiElement expression) {
        String text = document.getText(new TextRange(expression.getTextRange().getStartOffset(), expression.getTextRange().getEndOffset()));
        System.out.println("text = " + text);
        return text;
    }

    private static @NotNull String getCurrentLineText2(@NotNull Editor editor, Document document) {
        int offset = editor.getCaretModel().getOffset();
        int lineNumber = document.getLineNumber(offset);
        int lineStartOffset = document.getLineStartOffset(lineNumber);
        int lineEndOffset = document.getLineEndOffset(lineNumber);
        String currentLineText = document.getText(new TextRange(lineStartOffset, lineEndOffset));
        return currentLineText;
    }
    // GPT给的一个判断方法
    // @Override
    // public boolean isApplicable(@NotNull PsiFile file, int offset) {
    //     // 获取当前光标位置的元素
    //     PsiElement elementAtCaret = file.findElementAt(offset);
    //
    //     // 检查光标是否位于方法调用之后
    //     if (elementAtCaret != null && elementAtCaret.getParent() instanceof PsiMethodCallExpression) {
    //         return true;
    //     }
    //
    //     // 检查光标是否位于变量声明之后
    //     if (elementAtCaret != null && elementAtCaret.getParent() instanceof PsiLocalVariable) {
    //         return true;
    //     }
    //
    //     // 检查光标是否位于循环语句之后
    //     if (elementAtCaret != null && (elementAtCaret.getParent() instanceof PsiForStatement ||
    //             elementAtCaret.getParent() instanceof PsiWhileStatement ||
    //             elementAtCaret.getParent() instanceof PsiDoWhileStatement)) {
    //         return true;
    //     }
    //
    //     // 检查光标是否位于代码行的末尾
    //     if (elementAtCaret != null && elementAtCaret.getText().trim().endsWith(";")) {
    //         return true;
    //     }
    //
    //     // 其他条件...
    //
    //     // 如果以上条件都不满足，则认为不适用
    //     return false;
    // }


}
