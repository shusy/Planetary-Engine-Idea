package com.asteroid.planetary_engine.idea.ui;

import cn.hutool.core.bean.BeanUtil;
import com.asteroid.planetary_engine.idea.domain.EntityCSMType;
import com.asteroid.planetary_engine.idea.state.EntityGenerateStateManage;
import com.asteroid.planetary_engine.idea.utils.highlight.MyTemplateHighlighter;
import com.intellij.codeInsight.daemon.DaemonCodeAnalyzer;
import com.intellij.codeInsight.template.impl.TemplateEditorUtil;
import com.intellij.ide.fileTemplates.impl.FileTemplateHighlighter;
import com.intellij.ide.highlighter.JavaFileHighlighter;
import com.intellij.ide.highlighter.JavaFileType;
import com.intellij.lang.java.JavaLanguage;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.editor.colors.EditorColorsManager;
import com.intellij.openapi.editor.colors.EditorColorsScheme;
import com.intellij.openapi.editor.ex.EditorEx;
import com.intellij.openapi.editor.ex.util.LayerDescriptor;
import com.intellij.openapi.editor.ex.util.LayeredLexerEditorHighlighter;
import com.intellij.openapi.fileTypes.PlainSyntaxHighlighter;
import com.intellij.openapi.fileTypes.SyntaxHighlighter;
import com.intellij.openapi.fileTypes.SyntaxHighlighterFactory;
import com.intellij.openapi.options.Configurable;
import com.intellij.openapi.options.ConfigurationException;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.project.ProjectManager;
import com.intellij.openapi.util.NlsContexts.ConfigurableName;
import com.intellij.psi.JavaCodeFragment;
import com.intellij.psi.JavaCodeFragmentFactory;
import com.intellij.psi.JavaPsiFacade;
import com.intellij.psi.PsiDocumentManager;
import com.intellij.psi.tree.IElementType;
import com.intellij.ui.components.JBList;
import lombok.Data;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.util.List;

/**
 * @author shuyang
 * @since 2024/4/16 21:07
 */
@Data
public class EntityGenerateCSMUI implements Configurable {

    private JPanel rootPanel;
    private JPanel mvcPanel;
    private JPanel configPanel;
    private JPanel commonPanel;
    private JPanel itemPanel;
    private JTextField packageName;
    private JTextField annotationName;
    private JBList<EntityCSMType> entityTypeList;
    private JPanel itemListPanel;
    private JPanel templatePanel;
    private JSplitPane jSplitPane;
    private JScrollPane editorScrollPanel;
    private JPanel editorPanel;
    private EntityGenerateStateManage.EntityGenerateState state = new EntityGenerateStateManage.EntityGenerateState();
    private Editor myTemplateEditor;

    {
        EntityGenerateStateManage stateManage = EntityGenerateStateManage.getInstance();
        BeanUtil.copyProperties(stateManage.getState(), state);
        packageName.setText(state.getPackageName());
        annotationName.setText(state.getAnnotationQualifiedName());
    }

    @Override
    public @Nullable @NonNls String getHelpTopic() {
        return "OH!!! Help me!!!";
    }

    @Override
    public @ConfigurableName String getDisplayName() {
        return "PlanetaryEngine";
    }

    @Override
    public @Nullable JComponent createComponent() {
        // this.reset();
        return rootPanel;
    }

    @Override
    public boolean isModified() {
        return !state.equals(EntityGenerateStateManage.DEFAULT_STATE);
    }

    @Override
    public void apply() throws ConfigurationException {
        EntityGenerateStateManage.update(state);
    }

    public EntityGenerateCSMUI() {
        this.initEditor();
        packageName.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                state.setPackageName(packageName.getText());
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                state.setPackageName(packageName.getText());
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                state.setPackageName(packageName.getText());
            }

        });
        annotationName.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                state.setAnnotationQualifiedName(annotationName.getText());
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                state.setAnnotationQualifiedName(annotationName.getText());
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                state.setAnnotationQualifiedName(annotationName.getText());
            }

        });
        DefaultListModel<EntityCSMType> model = new DefaultListModel<>();
        model.addAll(List.of(EntityCSMType.values()));
        entityTypeList.setModel(model);
        entityTypeList.addListSelectionListener(e -> {
            if (!e.getValueIsAdjusting()) { // 该检查确保事件不是在值正在调整时触发
                EntityCSMType entityCSMType = entityTypeList.getSelectedValue();
                changeItemState(entityCSMType);
            }
        });
        entityTypeList.setSelectedValue(model.firstElement(), false);
    }

    private synchronized void changeItemState(EntityCSMType entityCSMType) {
        String context = state.getConfigMap().get(entityCSMType);
        ApplicationManager.getApplication().runWriteAction(()->{
            myTemplateEditor.getDocument().setText(context);
            editorScrollPanel.getViewport().setViewPosition(new Point(0, 0));
        });

    }

    @Override
    public void reset() {
        EntityGenerateStateManage stateManage = EntityGenerateStateManage.getInstance();
        BeanUtil.copyProperties(stateManage.getState(), state);
        packageName.setText(state.getPackageName());
        annotationName.setText(state.getAnnotationQualifiedName());
    }

    private void initEditor() {
        myTemplateEditor = TemplateEditorUtil.createEditor(false, "");
        ((EditorEx) myTemplateEditor).setHighlighter(this.getJavaHighLight());
        ((EditorEx) myTemplateEditor).setColorsScheme(EditorColorsManager.getInstance().getGlobalScheme());

        myTemplateEditor.getDocument().addDocumentListener(new com.intellij.openapi.editor.event.DocumentListener() {
            @Override
            public void documentChanged(@NotNull com.intellij.openapi.editor.event.DocumentEvent e) {
                System.out.println("documentChanged = " + e.getNewLength());
                // 获取当前选中的子项
                EntityCSMType entityCSMType = entityTypeList.getSelectedValue();
                if (entityCSMType != null) {
                    // 更新子项对应的 value
                    state.getConfigMap().put(entityCSMType, myTemplateEditor.getDocument().getText());
                }
            }
        });
        editorPanel.add(myTemplateEditor.getComponent());
    }

    private static @Nullable Document test() {
        Project project = ProjectManager.getInstance().getDefaultProject();
        final JavaPsiFacade psiFacade = JavaPsiFacade.getInstance(project);
        final JavaCodeFragmentFactory factory = JavaCodeFragmentFactory.getInstance(project);
        final JavaCodeFragment fragment = factory.createCodeBlockCodeFragment(EntityGenerateStateManage.getInstance().getState().getConfigMap().get(EntityCSMType.Controller), psiFacade.findPackage(""), true);
        DaemonCodeAnalyzer.getInstance(project).setHighlightingEnabled(fragment, false);
        return PsiDocumentManager.getInstance(project).getDocument(fragment);
    }

    private LayeredLexerEditorHighlighter getJavaHighLight() {
        SyntaxHighlighter originalHighlighter = SyntaxHighlighterFactory.getSyntaxHighlighter(JavaFileType.INSTANCE, null, null);
        if (originalHighlighter == null) {
            originalHighlighter = new JavaFileHighlighter();
        }
        final EditorColorsScheme scheme = EditorColorsManager.getInstance().getGlobalScheme();
        LayeredLexerEditorHighlighter highlighter;
        // 识别Java语言
        highlighter = new LayeredLexerEditorHighlighter(new JavaFileHighlighter(), scheme);
        highlighter.registerLayer(new IElementType("java.FILE", JavaLanguage.INSTANCE), new LayerDescriptor(originalHighlighter, ""));
        return highlighter;
    }

    private LayeredLexerEditorHighlighter getFileHighLight() {
        SyntaxHighlighter originalHighlighter = SyntaxHighlighterFactory.getSyntaxHighlighter(JavaFileType.INSTANCE, null, null);
        if (originalHighlighter == null) {
            originalHighlighter = new PlainSyntaxHighlighter();
        }
        final EditorColorsScheme scheme = EditorColorsManager.getInstance().getGlobalScheme();
        LayeredLexerEditorHighlighter highlighter;
        // 识别文件模版
        highlighter = new LayeredLexerEditorHighlighter(new FileTemplateHighlighter(), scheme);
        highlighter.registerLayer(new IElementType("java.FILE", JavaLanguage.INSTANCE), new LayerDescriptor(originalHighlighter, ""));
        return highlighter;
    }

    private LayeredLexerEditorHighlighter getOtherHighLight() {
        SyntaxHighlighter originalHighlighter = SyntaxHighlighterFactory.getSyntaxHighlighter(JavaFileType.INSTANCE, null, null);
        if (originalHighlighter == null) {
            originalHighlighter = new JavaFileHighlighter();
        }
        final EditorColorsScheme scheme = EditorColorsManager.getInstance().getGlobalScheme();
        LayeredLexerEditorHighlighter highlighter;
        // 那怎么识别Java语言的文件模版呢？
        highlighter = new LayeredLexerEditorHighlighter(new MyTemplateHighlighter(), scheme);
        // LexerEditorHighlighter editorHighlighter = new LexerEditorHighlighter(new JavaFileHighlighter(LanguageLevel.JDK_19), scheme);
        // highlighter = new LayeredLexerEditorHighlighter(new HtmlFileHighlighter(), scheme);
        highlighter.registerLayer(new IElementType("java.FILE", JavaLanguage.INSTANCE), new LayerDescriptor(originalHighlighter, ""));
        return highlighter;
    }
}
