package com.asteroid.planetary_engine.idea.action;

import cn.hutool.json.JSONUtil;
import com.asteroid.planetary_engine.idea.domain.EntityCSMType;
import com.asteroid.planetary_engine.idea.state.EntityGenerateStateManage;
import com.asteroid.planetary_engine.idea.utils.ClassCreateUtil;
import com.asteroid.planetary_engine.idea.utils.NotifyUtil;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.MessageType;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiAnnotation;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiJavaFile;
import com.intellij.psi.PsiManager;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class EntityGenerateAction extends AnAction {

    private final EntityGenerateStateManage.EntityGenerateState generateState = EntityGenerateStateManage.getInstance().getState();

    @Override
    public void actionPerformed(@NotNull AnActionEvent anActionEvent) {
        // 获取当前项目
        Project project = anActionEvent.getProject();
        if (null == project) {
            return;
        }
        VirtualFile[] virtualFiles = anActionEvent.getData(CommonDataKeys.VIRTUAL_FILE_ARRAY);
        if (virtualFiles == null || virtualFiles.length == 0) {
            NotifyUtil.notify("未找到.java文件", MessageType.INFO);
            return;
        }
        Set<PsiJavaFile> psiJavaFiles = findPsiJavaFile(project, virtualFiles);
        if (psiJavaFiles.isEmpty()) {
            NotifyUtil.notify("未找到.java文件", MessageType.INFO);
            return;
        }
        if (StringUtils.isEmpty(generateState.getPackageName())) {
            NotifyUtil.notify("起始包名未配置", MessageType.WARNING);
            return;
        }
        if (StringUtils.isEmpty(generateState.getAnnotationQualifiedName())) {
            NotifyUtil.notify("注解的全限定名称未配置", MessageType.WARNING);
            return;
        }
        for (PsiJavaFile psiJavaFile : psiJavaFiles) {
            generateByPsiJavaFile(project, psiJavaFile);
        }
    }

    private void generateByPsiJavaFile(Project project, PsiJavaFile psiJavaFile) {
        PsiClass[] classes = psiJavaFile.getClasses();
        if (classes.length == 0) {
            NotifyUtil.notify("当前Java文件没有Class信息", MessageType.WARNING);
            return;
        }
        PsiClass psiClass = classes[0];
        if (psiClass == null) {
            NotifyUtil.notify("当前文件不是.java文件", MessageType.WARNING);
            return;
        }
        String packageName = generateState.getPackageName();
        String annotationQualifiedName = generateState.getAnnotationQualifiedName();
        HashMap<EntityCSMType, String> configMap = generateState.getConfigMap();

        PsiAnnotation annotation = psiClass.getAnnotation(annotationQualifiedName);
        if (annotation == null) {
            // Messages.showMessageDialog(psiJavaFile.getName() + "未标注@Entity注解", "未找到指定注解", Messages.getWarningIcon());
            NotifyUtil.notify(psiJavaFile.getName() + "未标注@Entity注解", MessageType.WARNING);
            // Messages.showMessageDialog("未标注@com.example.emptydemo.at.Entity注解", "未找到指定注解",  UIUtil.getWarningIcon());
            return;
        }
        if (StringUtils.isEmpty(packageName)) {
            return;
        }
        for (Map.Entry<EntityCSMType, String> entry : configMap.entrySet()) {
            EntityCSMType entityCSMType = entry.getKey();
            String content = entry.getValue();
            HashMap<String, String> replaceKV = new HashMap<>();
            replaceKV.put("className", psiClass.getName());
            replaceKV.put("lowClassName", getLowClassName(psiClass.getName()));
            replaceKV.put("qualifiedName", psiClass.getQualifiedName());
            replaceKV.put("sourcePackage", psiJavaFile.getPackageName());
            replaceKV.put("startPackage", packageName);
            String targetClassName = psiClass.getName() + entityCSMType.name();
            replaceKV.put("targetClassName", targetClassName);
            System.out.println(JSONUtil.toJsonPrettyStr(replaceKV));
            // 将entityType.getCode()的第一个字符转为小写
            String code = entityCSMType.getCode();
            String name = getLowClassName(code);
            String realPackageName = packageName + "." + name;
            replaceKV.put("targetPackageName", realPackageName);
            content = "package " + realPackageName + ";\n\n" + content;
            for (Map.Entry<String, String> subEntry : replaceKV.entrySet()) {
                content = content.replace("${" + subEntry.getKey() + "}", subEntry.getValue() == null ? "" : subEntry.getValue());
            }
            System.out.println("content = " + content);
            ClassCreateUtil.generateClassFile(project, content, targetClassName + ".java", realPackageName, psiJavaFile);
        }
    }

    private Set<PsiJavaFile> findPsiJavaFile(Project project, VirtualFile[] virtualFiles) {
        Set<PsiJavaFile> list = new HashSet<>();
        for (VirtualFile virtualFile : virtualFiles) {
            if (virtualFile.isDirectory()) {
                VirtualFile[] children = virtualFile.getChildren();
                if (children != null && children.length > 0) {
                    list.addAll(findPsiJavaFile(project, children));
                }
            } else {
                if (virtualFile.getName().endsWith(".java")) {
                    System.out.println(virtualFile.getName() + "isJava");
                    list.add((PsiJavaFile) PsiManager.getInstance(project).findFile(virtualFile));
                }
            }
        }
        return list;
    }

    private String getLowClassName(@Nullable String name) {
        if (StringUtils.isEmpty(name)) {
            return "";
        }
        return name.substring(0, 1).toLowerCase() + name.substring(1);
    }

    private String getUpClassName(@Nullable String name) {
        if (StringUtils.isEmpty(name)) {
            return "";
        }
        return name.substring(0, 1).toUpperCase() + name.substring(1);
    }
}
