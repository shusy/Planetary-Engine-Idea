package com.asteroid.planetary_engine.idea.action;

import com.asteroid.planetary_engine.idea.ui.EntityGenerateCSMUI;
import com.intellij.notification.Notification;
import com.intellij.notification.NotificationGroup;
import com.intellij.notification.NotificationGroupManager;
import com.intellij.notification.Notifications;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.ui.MessageType;
import org.jetbrains.annotations.NotNull;

import java.nio.charset.Charset;

public class TestDemo extends AnAction {

    @Override
    public void actionPerformed(@NotNull AnActionEvent e) {
        System.out.println(Charset.defaultCharset().displayName());
        System.out.println("0信1息2信息3");
        NotificationGroupManager instance = NotificationGroupManager.getInstance();
        NotificationGroup codeDot = instance.getNotificationGroup("EntityGenerateNotifyGroup");
        Notification notification = codeDot.createNotification("Notify-测试通知", MessageType.INFO);
        // Messages.showErrorDialog(anActionEvent.getProject(), "请按规：先复制对象后，例如：A a，再光标放到需要织入的对象上，例如：B b！联系作者：小傅哥 微信：fustack", "错误提示");
        Notifications.Bus.notify(notification);
        // Project project = e.getProject();
        // String basePath = project.getBasePath();
        // try {
        //     // 读取文件
        //     PropertiesUtil.loadProperties(new FileReader(basePath));
        // } catch (IOException ex) {
        //     throw new RuntimeException(ex);
        // }
        // PropertiesComponent.getInstance().getValue()
        new EntityGenerateCSMUI();
    }
}
