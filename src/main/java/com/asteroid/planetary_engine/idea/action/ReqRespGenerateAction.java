package com.asteroid.planetary_engine.idea.action;

import com.asteroid.planetary_engine.idea.utils.ClassCreateUtil;
import com.asteroid.planetary_engine.idea.utils.NotifyUtil;
import com.intellij.lang.jvm.JvmParameter;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.MessageType;
import com.intellij.psi.*;
import com.intellij.psi.codeStyle.CodeStyleManager;
import com.intellij.psi.codeStyle.JavaCodeStyleManager;
import com.intellij.psi.impl.source.PsiImportListImpl;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.util.IncorrectOperationException;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.Nullable;

import java.util.HashSet;
import java.util.Set;

public class ReqRespGenerateAction extends AnAction {

    public static final String startPackage = "com.cqkqinfo.hospital_shop.pojo.vo";

    @Override
    public void actionPerformed(AnActionEvent e) {
        Project project = e.getProject();
        // 获取当前编辑器实例
        Editor editor = e.getData(CommonDataKeys.EDITOR);
        if (editor == null) return;

        // 获取当前文件
        PsiFile psiFile = e.getData(CommonDataKeys.PSI_FILE);
        if (psiFile == null) return;

        // 获取光标位置
        int offset = editor.getCaretModel().getOffset();

        // 根据光标位置找到对应的PsiElement
        PsiElement elementAt = psiFile.findElementAt(offset);
        if (elementAt == null) return;

        // 查找最近的PsiMethod
        PsiMethod method = PsiTreeUtil.getParentOfType(elementAt, PsiMethod.class);
        if (method != null) {
            // 在这里处理选中的方法，例如显示信息、执行操作等
            System.out.println("Selected Method: " + method.getName());
            PsiAnnotation[] annotations = method.getAnnotations();
            for (PsiAnnotation annotation : annotations) {
                if (allowSet.contains(annotation.getQualifiedName())) {
                    String name = method.getName();
                    String reqName = getUpClassName(name) + "Req";
                    String reqPack = startPackage + ".req";
                    String reqContent = """

                            import lombok.Data;

                            @Data
                            public class\s""" + reqName + """
                            {

                            }""";
                    ClassCreateUtil.generateClassFile(project, reqContent, reqName + ".java", reqPack, psiFile);
                    PsiType reqPsiType = JavaPsiFacade.getInstance(project).getElementFactory().createTypeFromText(reqPack + "." + reqName, method);
                    PsiParameter newParam = JavaPsiFacade.getInstance(project).getElementFactory().createParameter(getLowClassName(reqName), reqPsiType);
                    WriteCommandAction.runWriteCommandAction(project, () -> {
                        method.getParameterList().add(newParam);
                    });
                    String respName = getUpClassName(name) + "Resp";
                    String respPack = startPackage + ".resp";
                    String respContent = """

                            import lombok.Data;

                            @Data
                            public class\s""" + respName + """
                            {

                            }""";
                    PsiFile respPsiFile = ClassCreateUtil.generateClassFile(project, respContent, respName + ".java", respPack, psiFile);
                    PsiTypeElement typeElement = JavaPsiFacade.getInstance(project)
                            .getElementFactory()
                            .createTypeElementFromText(respPack + "." + respName, method);
                    PsiTypeElement returnTypeElement = method.getReturnTypeElement();
                    WriteCommandAction.runWriteCommandAction(project, () -> {
                        PsiElementFactory factory = JavaPsiFacade.getInstance(project).getElementFactory();
                        // 使用全限定类名创建导入语句
                        PsiImportStatement importStatement = factory.createImportStatement(
                                factory.createTypeByFQClassName(respPack + "." + respName, GlobalSearchScope.allScope(project)).resolve()
                        );

                        // PsiImportStatement importStatement = factory.createImportStatementOnDemand(respPack + "." + respName);

                        // 获取文件的导入列表
                        PsiImportList importList = ((PsiJavaFile) method.getContainingFile()).getImportList();
                        // 添加新的导入语句到导入列表
                        importList.add(importStatement);

                        returnTypeElement.replace(typeElement);
                        // 优化导入
                        PsiDocumentManager documentManager = PsiDocumentManager.getInstance(project);
                        PsiFile containingFile = method.getContainingFile();
                        Document document = documentManager.getDocument(containingFile);
                        if (document != null) {
                            CodeStyleManager codeStyleManager = CodeStyleManager.getInstance(project);
                            codeStyleManager.reformat(containingFile);
                            documentManager.commitDocument(document);
                        }
                    });

                }
            }


        } else {
            // 没有选中方法的情况处理
        }
    }

    private static final Set<String> allowSet = new HashSet<>();

    static {
        allowSet.add("org.springframework.web.bind.annotation.GetMapping");
        allowSet.add("org.springframework.web.bind.annotation.DeleteMapping");
        allowSet.add("org.springframework.web.bind.annotation.PostMapping");
        allowSet.add("org.springframework.web.bind.annotation.PutMapping");
        allowSet.add("org.springframework.web.bind.annotation.RequestMapping");
    }


    // public static void addParameterToMethod(PsiMethod method, String parameterTypeText, String parameterName, Project project) {
    //     // 获取PsiElementFactory用于创建新的Psi元素
    //     PsiElementFactory factory = JavaPsiFacade.getElementFactory(project);
    //
    //     PsiType parameterType = PsiTypesUtil.getPsiType(project, parameterTypeText);
    //     // 创建新的参数
    //     PsiParameter newParameter = factory.createParameter(parameterTypeText + " " + parameterName, JvmParameter.class,method);
    //
    //     // 获取方法的参数列表
    //     PsiParameterList parameterList = method.getParameterList();
    //
    //     // 插入新参数到参数列表的末尾
    //     parameterList.addAfter(newParameter, parameterList.getLastParameter());
    //
    //     // 重新格式化代码以保持良好的格式
    //     JavaCodeStyleManager codeStyleManager = JavaCodeStyleManager.getInstance(project);
    //     codeStyleManager.shortenClassReferences(method);
    //     codeStyleManager.reformat(method);
    // }
    private static String getUpClassName(@Nullable String name) {
        if (StringUtils.isEmpty(name)) {
            return "";
        }
        return name.substring(0, 1).toUpperCase() + name.substring(1);
    }

    private String getLowClassName(@Nullable String name) {
        if (StringUtils.isEmpty(name)) {
            return "";
        }
        return name.substring(0, 1).toLowerCase() + name.substring(1);
    }
}
