package com.asteroid.planetary_engine.idea.domain;

import lombok.Getter;

/**
 * @author shuyang
 * @since 2024/4/16 0:14
 */
@Getter
public enum EntityCSMType {
    Controller("controller"),
    Service("service"),
    ServiceImpl("service.impl"),
    Mapper("mapper"),
    ;

    private final String code;

    EntityCSMType(String code) {
        this.code = code;
    }
}
