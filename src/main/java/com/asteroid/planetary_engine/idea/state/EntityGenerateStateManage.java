package com.asteroid.planetary_engine.idea.state;

import cn.hutool.json.JSONUtil;
import com.asteroid.planetary_engine.idea.domain.EntityCSMType;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.components.PersistentStateComponent;
import com.intellij.openapi.components.Service;
import com.intellij.openapi.components.State;
import com.intellij.openapi.components.Storage;
import lombok.Data;
import org.apache.commons.io.IOUtils;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

/**
 * 注册应用service，类似于注册Bean
 * 使用注解注册 {@link Service}
 * 或者在plugin.xml中增加
 * <applicationService serviceImplementation="com.asteroid.planetary_engine.idea.state.EntityGenerateStateManage"/>
 *
 * @author shuyang
 * @since 2024/4/19 11:15
 */
@State(
        name = "com.asteroid.planetary_engine.idea.setting.EntityGenerateSettingManage",
        storages = @Storage("EntityGenerateSetting.xml")
)
@Service(Service.Level.APP)
public final class EntityGenerateStateManage implements PersistentStateComponent<EntityGenerateStateManage.EntityGenerateState> {

    public static EntityGenerateState DEFAULT_STATE = null;

    public static EntityGenerateStateManage getInstance() {
        return ApplicationManager.getApplication().getService(EntityGenerateStateManage.class);
    }

    public static void update(EntityGenerateState state) {
        DEFAULT_STATE.setPackageName(state.getPackageName());
        DEFAULT_STATE.setAnnotationQualifiedName(state.getAnnotationQualifiedName());
        DEFAULT_STATE.configMap.clear();
        DEFAULT_STATE.configMap.putAll(state.configMap);
    }


    @Override
    public EntityGenerateStateManage.@NotNull EntityGenerateState getState() {
        if (DEFAULT_STATE == null) {
            InputStream inputStream = EntityGenerateStateManage.class.getClassLoader()
                    .getResourceAsStream("EntityGenerate.json");
            try {
                String string = IOUtils.toString(inputStream);
                DEFAULT_STATE = JSONUtil.toBean(string, EntityGenerateState.class);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return DEFAULT_STATE;
    }

    @Override
    public void loadState(@NotNull EntityGenerateStateManage.EntityGenerateState state) {
        DEFAULT_STATE = state;
    }

    @Data
    public static class EntityGenerateState {
        /**
         * 起始包名
         */
        private String packageName;
        /**
         * 识别的注解的全限定名称
         */
        private String annotationQualifiedName;
        /**
         * 配置项
         */
        private HashMap<EntityCSMType, String> configMap = new HashMap<>();
    }
}
