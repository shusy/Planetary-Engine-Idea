package com.asteroid.planetary_engine.idea.provider;

import com.asteroid.planetary_engine.idea.template.GenerateLogPostfixTemplate;
import com.asteroid.planetary_engine.idea.template.GenerateJsonLogPostfixTemplate;
import com.intellij.codeInsight.template.postfix.templates.PostfixTemplate;
import com.intellij.codeInsight.template.postfix.templates.PostfixTemplateProvider;
import com.intellij.openapi.editor.Editor;
import com.intellij.psi.PsiFile;
import org.jetbrains.annotations.NotNull;

import java.util.Set;

/**
 * @author shuyang
 * @since 2024/7/18 22:01
 */
public class GenerateLogPostfixTemplateProvider implements PostfixTemplateProvider {
    @Override
    public @NotNull Set<PostfixTemplate> getTemplates() {
        return Set.of(new GenerateLogPostfixTemplate(this),
                new GenerateJsonLogPostfixTemplate(this)
        );
    }

    @Override
    public boolean isTerminalSymbol(char currentChar) {
        return currentChar == '.';
    }


    @Override
    public void preExpand(@NotNull PsiFile file, @NotNull Editor editor) {

    }

    @Override
    public void afterExpand(@NotNull PsiFile file, @NotNull Editor editor) {

    }

    @Override
    public @NotNull PsiFile preCheck(@NotNull PsiFile copyFile, @NotNull Editor realEditor, int currentOffset) {
        return copyFile;
    }
}
